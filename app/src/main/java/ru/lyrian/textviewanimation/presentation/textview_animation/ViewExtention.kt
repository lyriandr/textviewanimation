package ru.lyrian.textviewanimation.presentation.textview_animation

import android.view.View
import android.view.ViewPropertyAnimator
import android.view.animation.LinearInterpolator

fun View.provideDefaultAnimator(): ViewPropertyAnimator =
    this
        .animate()
        .setStartDelay(0L)
        .setDuration(0L)
        .setInterpolator(LinearInterpolator())
        .setListener(null)