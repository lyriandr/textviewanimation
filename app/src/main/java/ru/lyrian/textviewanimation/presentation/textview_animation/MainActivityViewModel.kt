package ru.lyrian.textviewanimation.presentation.textview_animation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.lyrian.textviewanimation.presentation.OneTimeValue

class MainActivityViewModel : ViewModel() {
    private var isAnimationPostponed = true
    private val _moveToCoordinatesLD = MutableLiveData<OneTimeValue<Pair<Float, Float>>>()
    val moveToCoordinatesLD: LiveData<OneTimeValue<Pair<Float, Float>>>
        get() = _moveToCoordinatesLD

    private val _moveDownLD = MutableLiveData<OneTimeValue<Boolean>>()
    val moveDownLD: LiveData<OneTimeValue<Boolean>>
        get() = _moveDownLD

    private val _moveUpLD = MutableLiveData<OneTimeValue<Boolean>>()
    val moveUpLD: LiveData<OneTimeValue<Boolean>>
        get() = _moveUpLD

    fun moveViewToCoordinates(touchCoordinates: Pair<Float, Float>) {
        this.isAnimationPostponed = true
        this._moveToCoordinatesLD.value = OneTimeValue(touchCoordinates)
    }

    fun performMoveDownAnimation() {
        this._moveDownLD.value = OneTimeValue(this.isAnimationPostponed)
        this.isAnimationPostponed = false
    }

    fun performMoveUpAnimation() {
        this._moveUpLD.value = OneTimeValue(true)
    }
}