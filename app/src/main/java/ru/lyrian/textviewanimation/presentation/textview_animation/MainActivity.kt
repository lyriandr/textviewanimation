package ru.lyrian.textviewanimation.presentation.textview_animation

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import ru.lyrian.textviewanimation.R
import ru.lyrian.textviewanimation.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val firstMoveAnimationDelay = 5000L
    private val upAndDownAnimationDuration = 10000L
    private val mainActivityViewModel: MainActivityViewModel by viewModels()
    private val parentLayoutSize: Pair<Float, Float> by lazy {
        Pair(
            this.binding.clTextViewContainer.width.toFloat(),
            this.binding.clTextViewContainer.height.toFloat()
        )
    }

    private val animatedViewSize: Pair<Float, Float> by lazy {
        Pair(
            this.binding.tvAnimatedTextView.width.toFloat(),
            this.binding.tvAnimatedTextView.height.toFloat()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(this.binding.root)
    }

    override fun onStart() {
        super.onStart()

        setOnTouchListeners()
        subscribeOnLiveData()
        setOnClickListeners()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setOnTouchListeners() {
        this.binding.clTextViewContainer.setOnTouchListener { _, event ->
            val touchCoordinates = Pair(event.x, event.y)

            this.mainActivityViewModel.moveViewToCoordinates(touchCoordinates)
            false
        }
    }

    private fun subscribeOnLiveData() {
        this.mainActivityViewModel.moveToCoordinatesLD.observe(this) {
            it.getValueIfNotRequested()?.let { moveCoordinates: Pair<Float, Float> ->
                moveViewToCoordinates(moveCoordinates)
            }
        }

        this.mainActivityViewModel.moveDownLD.observe(this) {
            it.getValueIfNotRequested()?.let { isAnimationPostponed: Boolean ->
                if (isAnimationPostponed) {
                    performMoveDownAnimation(this.firstMoveAnimationDelay)
                } else {
                    performMoveDownAnimation()
                }
            }
        }

        this.mainActivityViewModel.moveUpLD.observe(this) {
            it.getValueIfNotRequested()?.let {
                performMoveUpAnimation()
            }
        }
    }

    private fun setOnClickListeners() {
        this.binding.tvAnimatedTextView.setOnClickListener {
            it.provideDefaultAnimator().cancel()
        }
    }

    private fun moveViewToCoordinates(moveCoordinates: Pair<Float, Float>) {
        val xMoveCoordinate = moveCoordinates.first - animatedViewSize.first / 2
        val yMoveCoordinate = moveCoordinates.second - animatedViewSize.second / 2

        val onAnimationEndListener = object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)

                mainActivityViewModel.performMoveDownAnimation()
            }
        }

        this.binding.tvAnimatedTextView
            .provideDefaultAnimator()
            .x(xMoveCoordinate)
            .y(yMoveCoordinate)
            .setListener(onAnimationEndListener)
            .start()

        this.binding.tvAnimatedTextView.setTextColor(ContextCompat.getColor(this, R.color.text_view_color))
    }

    private fun performMoveDownAnimation(startDelay: Long = 0) {
        val onAnimationEndListener = object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)

                mainActivityViewModel.performMoveUpAnimation()
            }
        }

        this.binding.tvAnimatedTextView
            .provideDefaultAnimator()
            .setStartDelay(startDelay)
            .setDuration(this.upAndDownAnimationDuration)
            .setListener(onAnimationEndListener)
            .y(parentLayoutSize.second - animatedViewSize.second)
    }

    private fun performMoveUpAnimation() {
        val onAnimationEndListener = object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)

                mainActivityViewModel.performMoveDownAnimation()
            }
        }

        this.binding.tvAnimatedTextView
            .provideDefaultAnimator()
            .setDuration(this.upAndDownAnimationDuration)
            .setListener(onAnimationEndListener)
            .y(0F)
    }
}


