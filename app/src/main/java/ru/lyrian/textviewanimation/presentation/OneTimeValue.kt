package ru.lyrian.textviewanimation.presentation

class OneTimeValue<T>(private val value: T) {
    private var valueAlreadyRequested = false

    fun getValueIfNotRequested(): T? =
        if (valueAlreadyRequested) {
            null
        } else {
            valueAlreadyRequested = true
            value
        }
}
